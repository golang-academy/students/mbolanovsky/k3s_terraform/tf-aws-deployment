# This is my first EC2 Instance
resource "aws_instance" "web" {
  count                  = 1
  ami                    = var.image
  instance_type          = var.instance
  subnet_id              = aws_subnet.dmz.id
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = "10.0.1.69"
  user_data              = file("postinstall.sh")

  tags = {
    Name  = "web${count.index + 1}"
    Owner = var.owner
  }
}
